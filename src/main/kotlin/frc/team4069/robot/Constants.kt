package frc.team4069.robot

object Constants {
    const val DRIVETRAIN_P = 0.25
    const val DRIVETRAIN_D = 0.0001
    const val DRIVETRAIN_V = 0.07143
    const val DRIVETRAIN_S = 0.1
    const val DRIVETRAIN_WIDTH_FT = 4.2
    // Ramsete constants
    const val kZeta = 0.95
    const val kB = 0.18
}